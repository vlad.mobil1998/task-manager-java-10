package ru.amster.tm.api;

public interface ICommandController {

    void showHelpMsg();

    void showArgMsg();

    void showCmdMsg();

    void showInfoMsg();

    void showAboutMsg();

    void showVersionMsg();

    void exit();

}
