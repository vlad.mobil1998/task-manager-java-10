package ru.amster.tm.api;

import ru.amster.tm.model.TerminalCommand;

public interface ICommandService {

    TerminalCommand[] getTerminalCommands();

    String[] getCOMMANDS();

    String[] getARGUMENTS();

}
