package ru.amster.tm.api;

import ru.amster.tm.model.TerminalCommand;


public interface ICommandRepository {

    String[] getCommands(TerminalCommand... value);

    String[] getArguments(TerminalCommand... value);

    TerminalCommand[] getTerminalCommands();

    String[] getCOMMANDS();

    String[] getARGUMENTS();

}
