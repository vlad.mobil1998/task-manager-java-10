package ru.amster.tm.bootstrap;

import ru.amster.tm.api.ICommandRepository;
import ru.amster.tm.api.ICommandService;
import ru.amster.tm.api.ICommandController;
import ru.amster.tm.constant.ProgramArgConst;
import ru.amster.tm.constant.TerminalCmdConst;
import ru.amster.tm.controller.CommandController;
import ru.amster.tm.repository.CommandRepository;
import ru.amster.tm.service.CommandService;

import java.util.Scanner;

public class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    public final void run(final String[] args){
        System.out.println("**** WELCOME TO TASK MANAGER ****");
        if (parseArgs(args)) System.exit(0);
        while (true) {
            final Scanner scanner = new Scanner(System.in);
            final String readTerminalCommand = scanner.nextLine();
            parseCmd(readTerminalCommand);
        }
    }

    private void parseCmd(String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case TerminalCmdConst.HELP:
                commandController.showHelpMsg();
                break;
            case TerminalCmdConst.ABOUT:
                commandController.showAboutMsg();
                break;
            case TerminalCmdConst.VERSION:
                commandController.showVersionMsg();
                break;
            case TerminalCmdConst.INFO:
                commandController.showInfoMsg();
                break;
            case TerminalCmdConst.ARGUMENTS:
                commandController.showArgMsg();
                break;
            case TerminalCmdConst.COMMANDS:
                commandController.showCmdMsg();
                break;
            case TerminalCmdConst.EXIT:
                commandController.exit();
                break;
            default:
                System.out.println("ERROR: Command not late");
                break;
        }
    }

    private boolean parseArgs(final String[] args) {
        if (args == null || args.length == 0) return false;
        for (int i = 0; i < args.length; i += 1) {
            System.out.println(" ");
            switch (args[i]) {
                case ProgramArgConst.HELP:
                    commandController.showHelpMsg();
                    break;
                case ProgramArgConst.ABOUT:
                    commandController.showAboutMsg();
                    break;
                case ProgramArgConst.VERSION:
                    commandController.showVersionMsg();
                    break;
                case ProgramArgConst.INFO:
                    commandController.showInfoMsg();
                    break;
                default:
                    System.out.println("ERROR: Argument not late");
                    break;
            }
        }
        return true;
    }

}
