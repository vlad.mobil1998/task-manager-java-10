package ru.amster.tm.service;

import ru.amster.tm.api.ICommandService;
import ru.amster.tm.model.TerminalCommand;
import ru.amster.tm.api.ICommandRepository;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    public TerminalCommand[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

    public String[] getCOMMANDS() {
        return commandRepository.getCOMMANDS();
    }

    public String[] getARGUMENTS() {
        return commandRepository.getARGUMENTS();
    }
}
